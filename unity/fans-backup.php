<?php
            $nameErrMsg = "";
            $emailErrMsg = "";   
            $respErrMsg = "";   
            $validForm = false;
            $inName = "";
            $inEmail = "";    
            $inCity = "";
            $fanName = "";
            $fanEmail = "";
            $fanCity = "";

            function validateName() {
              global $fanName, $validForm, $nameErrMsg;    

              $nameErrMsg = "";                
              $fanName = trim($fanName);

                      if($fanName=="")     
                      {
                        $validForm = false;         
                        $nameErrMsg = "Name Is Required"; 
                      }

                      if (!preg_match("/^[a-zA-Z ]*$/",$fanName)) {
                        $validForm = false;
                        $nameErrMsg = "Only letters and white space allowed"; 
                      } 
                    }

            function validateEmail()     
            {
              global $fanEmail, $validForm, $emailErrMsg;    

              $fanEmail = trim($fanEmail);

                      if($fanEmail=="")     
                      {
                        $validForm = false;         
                        $emailErrMsg = "Email Address Is Required"; 
                      }
                      if (!filter_var($fanEmail, FILTER_VALIDATE_EMAIL)) {
                        $emailErrMsg = "Invalid email format"; 
                      }
                    }

            function validateResp()   
            {
              global $fanCity, $validForm, $respErrMsg;  

              $respErrMsg = "";

                      if ($fanCity =="")
                        {
                        $validForm = false;
                        $respErrMsg = "Closest City Is Needed.";
                        } 
                    }

if  (isset($_POST['submit']))
        {
          include 'unityDbConnect.php';
          $fanName = $_POST['inName'];
          $fanEmail = $_POST['inEmail'];
          $fanCity = $_POST['inCity'];

          $validForm = true;

          validateName();
          validateEmail();
          validateResp();
          }
        ?>

        <?php

            if ($validForm)
              {
                   
                  $sqlHardCode = "INSERT INTO unity_fans (fanName, fanEmail, fanCity) VALUES (?,?,?);";

                  $stmt = $link->prepare($sqlHardCode); 

                  $stmt->bind_param("sss",$fanName,$fanEmail,$fanCity);

                      if  ( $stmt->execute()){
  
                      $message = '<h1>HUZZAH! You have joined the legions.</h1><h3>Click <a href="index.html">HERE</a> to return to the UNITY home page.</h3>';
                        }
                      else
                        {
                        $message = "<h1>You have encountered a big big problem.</h1>";
                        $message .= "<h2 style='color:red'>" . mysqli_error($link) . "</h2>"; //remove this for production purposes
                        }
            
                    $stmt->close();
                    $link->close(); 

            }
          ?>

        <!DOCTYPE HTML>
        <html>
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
          <title>WELCOME TO THE UNITY FAN PAGE</title>
          <style>
            .red  {
              color:red;
              font-style:italic;  
            }
          </style>
        </head>

        <body>
          <h1>WELCOME TO THE UNITY FAN PAGE</h1>
          <h2>Please enter your information below to join.</h2>
          <h5>Please note that the band and all of its entities and holdings values your privacy. We will engage in zero shenanigans when it comes to your personal information. Good day. </h5>

        <?php
        if($validForm)
          { 
     
          //If the form was submitted display the INSERT result message
          ?>
           <h3><?php echo($message); ?></h3>
          <?php
          }//end if
          else{
          ?>

      <header>Contact Form</header>
      <form name="form1" class="topBefore" method="post" action="fans.php">
        <p>&nbsp;</p>
        <p>
          <label>Your Name:
            <input type="text" name="inName" id="inName" value="<?php echo $fanName;?>"><p class="red"><?php echo "$nameErrMsg";?></p>
          </label>
        </p>
        <p>Your Email: 
          <input type="text" name="inEmail" id="inEmail" value="<?php echo $fanEmail;?>"/><p class="red"><?php echo "$emailErrMsg";?></p>
        </p>
        <p>Closest City To Me Is: <p class="red"><?php echo "$respErrMsg";?></p>
          <label>
            <select name="inCity" id="inCity">
              <option value="">Please Select a City</option>
              <option value="Des Moines"<?php if($fanCity == 'Des Moines'){echo("selected");}?>>Des Moines</option>
              <option value="Waterloo"<?php if($fanCity == 'Waterloo'){echo("selected");}?>>Waterloo</option>
              <option value="Cedar Rapids"<?php if($fanCity == 'Cedar Rapids'){echo("selected");}?>>Cedar Rapids</option>
            </select>
          </label>
        </p>
        <br>
        <p>
          <input type="submit" name="submit" id="submit" value="Submit">
          <input type="reset" name="reset" id="reset" value="Reset">
        </p>
      </form>

      <?php
      } //end else branch for the View area
      ?>

</body>
</html>
