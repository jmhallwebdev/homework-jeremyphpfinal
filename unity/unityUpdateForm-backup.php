<?php 
session_cache_limiter('none');
session_start();
 
  if ($_SESSION['validUser'] == "yes")
  {
          include 'unityDbConnect.php';       

          $nameErrMsg = "";
          $emailErrMsg = "";   
          $respErrMsg = "";   
          $validForm = false;
          $inName = "";
          $inEmail = "";    
          $inCity = "";
          $fanName = "";
          $fanEmail = "";
          $fanCity = "";
          $message = "";

          function validateName() {
              global $fanName, $validForm, $nameErrMsg;    

              $nameErrMsg = "";                
              $fanName = trim($fanName);

                      if($fanName=="")     
                      {
                        $validForm = false;         
                        $nameErrMsg = "Name Is Required"; 
                      }

                      if (!preg_match("/^[a-zA-Z ]*$/",$fanName)) {
                        $validForm = false;
                        $nameErrMsg = "Only letters and white space allowed"; 
                      } 
                    }

            function validateEmail()     
            {
              global $fanEmail, $validForm, $emailErrMsg;    

              $fanEmail = trim($fanEmail);

                      if($fanEmail=="")     
                      {
                        $validForm = false;         
                        $emailErrMsg = "Email Address Is Required"; 
                      }
                      if (!filter_var($fanEmail, FILTER_VALIDATE_EMAIL)) {
                        $emailErrMsg = "Invalid email format"; 
                      }
                    }

            function validateResp()   
            {
              global $fanCity, $validForm, $respErrMsg;  

              $respErrMsg = "";

                      if ($fanCity =="")
                        {
                        $validForm = false;
                        $respErrMsg = "Closest City Is Needed.";
                        } 
                    }

if  (isset($_POST['submit']))
        {
          include 'unityDbConnect.php';
          $fanName = $_POST['inName'];
          $fanEmail = $_POST['inEmail'];
          $fanCity = $_POST['inCity'];

          $validForm = true;

          validateName();
          validateEmail();
          validateResp();
          }

        ?>

        <?php
		
        if ($validForm)
              {
                $sql = "UPDATE unity_fans SET " ;
                $sql .= "fanName=?, ";
                $sql .= "fanEmail=?, ";
                $sql .= "fanCity=? ";  
                $sql .= " WHERE (fanNo='$fanNo')";
  
                $stmt = $con->prepare($sql);  //Prepare SQL query
  
                $stmt->bind_param("sss",$fanName,$fanEmail,$fanCity);
  
                    if ( $stmt->execute() )
                    {
                      $message = '<h1 class = "jersilver">HUZZAH! You have joined the legions.</h1><h3 class = "jersilver">Click <a href="index.html">HERE</a> to return to the UNITY home page.</h3>';
                        }
                    }
                    else
                    {
                      $message = "<h1>You have encountered a big big problem.</h1>";
                        $message .= "<h2 style='color:red'>" . mysqli_error($con) . "</h2>";
              } //end the if valid form TRUE area

              $stmt->close();
              $con->close(); 
        ?> 

        <!-- RESUME HERE --> 

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>WDV341 Intro PHP  - Event Admin Example</title>
    <style>
        .red  {
        color:red;
        font-style:italic;  
              }
    </style>
    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
    <link href="jquery-ui-timepicker-addon.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script src="jquery-ui-timepicker-addon.js"></script>
    <script>
      $(function() {
                $("#event_datepicker").datepicker({
                   dateFormat:"yy-mm-dd",
                });
             });
    </script>
    <script>
      $(function() {
                $('#event_timepicker').timepicker();
             });
    </script>
    <script type="text/javascript">
  function validateMyForm() {
    // The field is empty, submit the form.
    if(!document.getElementById("honeypot").value) { 
      return true;
    } 
     // the field has a value it's a spam bot
    else {
      return false;
    }
  }
</script>
</head>
<body>
    <h1>WDV341 Event Admin System Example</h1>
    <h3>Input Form for Adding Events</h3>



    <?php
    if($validForm)
      { 
       
       //If the form was submitted display the INSERT result message
      ?>
       <h3><?php echo($message); ?></h3>
      <?php
      }//end if

else
      {
        $updateEventId = $_GET['event_id'];
        //echo "<h1>updateEventId: $updateEventId</h1>";

        //  **** PROBLEMS BELOW ****

        $sql = "SELECT event_id,event_name,event_description,event_presenter,event_date,event_time FROM wdv341_event WHERE event_id=?"; 
          //echo "<p>The SQL Command: $sql </p>"; //For testing purposes as needed.

          $query = $link->prepare($sql);
    
          $query->bind_param("i",$updateEventId); 
          
        if( $query->execute() ) //Run Query and Make sure the Query ran correctly
          {
            $query->bind_result($event_id,$event_name,$event_description,$event_presenter,$event_date,$event_time);
      
            $query->store_result();
        
            $query->fetch();
          }
        else
          {
            $message = "<h1>You have encountered a problem with your update.</h1>";
            $message .= "<h2>" . mysqli_error($link) . "</h2>" ;      
          }
      ?>

      <p>This is the input form that allows the user/customer to enter the information for an Event. Once the form is submitted and validated it will call the eventsForm.php page. That page will pull the form data into the PHP and add a new record to the database.</p>


      <header>Event Form</header>
      <form id="updateEventForm" name="updateEventForm" method="post" action="updateEventForm.php" onsubmit="return validateMyForm();">
      <p>Add a new Event</p>
      <p>Event Name: 
      <input type="text" name="event_name" id="event_name" value="<?php echo $event_name;?>"></p><p class="red"><?php echo "$nameErrMsg";?></p>
      <p>Event Description:  
      <input type="text" name="event_description" id="event_description" value="<?php echo $event_description;?>"></p><p class="red"><?php echo "$descErrMsg";?></p>
      <p>Event Presenter: 
      <input type="text" name="event_presenter" id="event_presenter" value="<?php echo $event_presenter;?>"></p><p class="red"><?php echo "$presErrMsg";?></p>
      <p>Date: <input type="text" value="<?php echo $event_date;?>" name = "event_datepicker" id="event_datepicker"></p>
      <p class="red"><?php echo "$dateErrMsg";?></p>
      <p>Time: <input type="text" value="<?php echo $event_time;?>" name = "event_timepicker" id="event_timepicker"></p>
      <p class="red"><?php echo "$timeErrMsg";?></p>
      <p><input type="hidden" name="event_id" id="event_id"
      value="<?php echo $event_id;?>"/></p>
      <!--Honeypot is below -->
      <div style="display:none;">
      <label>Keep this field blank</label>
      <input type="text" name="honeypot" id="honeypot" />
      </div>
      <!--Honeypot is above -->

      <p>
      <input type="submit" name="submit" id="submit" value="Add Event" />
      <input type="reset" name="button2" id="button2" value="Clear Form" />
      </p>
      </form>

  <?php

      }

      ?>

    <?php
      }
  else                  //The user needs to log in.  Display the Login Form
  {
    
    header('Location: presentersLogin.php');
  }
  ?>

</body>
</html>
