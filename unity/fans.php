<?php
            $nameErrMsg = "";
            $emailErrMsg = "";   
            $respErrMsg = "";   
            $validForm = false;
            $inName = "";
            $inEmail = "";    
            $inCity = "";
            $fanName = "";
            $fanEmail = "";
            $fanCity = "";

            function validateName() {
              global $fanName, $validForm, $nameErrMsg;    

              $nameErrMsg = "";                
              $fanName = trim($fanName);

                      if($fanName=="")     
                      {
                        $validForm = false;         
                        $nameErrMsg = "Name Is Required"; 
                      }

                      if (!preg_match("/^[a-zA-Z ]*$/",$fanName)) {
                        $validForm = false;
                        $nameErrMsg = "Only letters and white space allowed"; 
                      } 
                    }

            function validateEmail()     
            {
              global $fanEmail, $validForm, $emailErrMsg;    

              $fanEmail = trim($fanEmail);

                      if($fanEmail=="")     
                      {
                        $validForm = false;         
                        $emailErrMsg = "Email Address Is Required"; 
                      }
                      if (!filter_var($fanEmail, FILTER_VALIDATE_EMAIL)) {
                        $emailErrMsg = "Invalid email format"; 
                      }
                    }

            function validateResp()   
            {
              global $fanCity, $validForm, $respErrMsg;  

              $respErrMsg = "";

                      if ($fanCity =="")
                        {
                        $validForm = false;
                        $respErrMsg = "Closest City Is Needed.";
                        } 
                    }

if  (isset($_POST['submit']))
        {
          include 'unityDbConnect.php';
          $fanName = $_POST['inName'];
          $fanEmail = $_POST['inEmail'];
          $fanCity = $_POST['inCity'];

          $validForm = true;

          validateName();
          validateEmail();
          validateResp();
          }
        ?>

        <?php

            if ($validForm)
              {
                   
                  $sqlHardCode = "INSERT INTO unity_fans (fanName, fanEmail, fanCity) VALUES (?,?,?);";

                  $stmt = $con->prepare($sqlHardCode); 

                  $stmt->bind_param("sss",$fanName,$fanEmail,$fanCity);

                      if  ( $stmt->execute()){
  
                      $message = '<h1 class = "jersilver">HUZZAH! You have joined the legions.</h1><h2 class="jersilver">You will receive a confirmation email shortly.</h2><h3 class = "jersilver">Click <a href="index.html">HERE</a> to return to the UNITY home page.</h3>';

                      require("unityEmail.php"); 
  
                      $newEmail = new myEmail($fanEmail, "midwestmetalhead@gmail.com", "NEW FAN SIGNUP", $fanCity); 

                      $newEmail->sendEmail();

                      $newEmailFan = new myEmail("From: info@unityinmetal.com", $fanEmail, "THE UNITY LEGIONS", "Thank you for joining the UNITY LEGIONS!"); 

                      $newEmailFan->sendEmail(); 

                        }
                      else
                        {
                        $message = "<h1>You have encountered a big big problem.</h1>";
                        $message .= "<h2 style='color:red'>" . mysqli_error($con) . "</h2>"; //remove this for production purposes
                        }
                    $stmt->close();
                    $con->close(); 
            }
          ?>

  <!DOCTYPE html>
  <html class="no-js" lang="en">
  <head>
  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>UNITY - FAN PAGE</title>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/normalize.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.min.css">
  <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="css/index.css">
  <link rel="stylesheet" href="css/animate.css">
  <style>
        .red  {
        color:red;
        font-style:italic; 
        }     

    </style>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
</head>
<body class="jerbackgroundspace">
  <nav class="top-bar jergradient1" data-topbar>
    <ul class="title-area">
      <li class="name">
        <img class="jernavpic jerpaddingleft animated fadeInLeft" src="images/smallunitylogo2.png"/> <a href="index.html"><span class="jersilver"> OFFICIAL WEBSITE</span></a>
      </li>
      <li class="toggle-topbar menu-icon"><a href="index.html"><span><h4 class="jersilver"></h4></span></a></li>
    </ul>
    <section class="top-bar-section">
      <ul class="right animated fadeInRight">
      <li class="divider"></li>
        <li>
          <a href="fans.php"><h4 class="jersilver">Fans</h4></a>
        </li>
        <li class="divider"></li>
        <li>
          <a href="music.html"><h4 class="jersilver">Music</h4></a>
        </li>
        <li class="divider"></li>
        <li><a href="band.html"><h4 class="jersilver">The Band</h4></a></li>
        <li class="divider"></li>
        <li>
          <a href="merch.html"><h4 class="jersilver">Merch</h4></a>
        </li>
        <li class="divider"></li>
        <li>
          <a href="unityLogin.php"><h4 class="jersilver">Admin Login</h4></a>
        </li>
      </ul>
    </section>
  </nav>
  <div class="row">
    <div class = "jermargins">

        <?php
        if($validForm)
          { 
          ?>
           <h3><?php echo($message); ?></h3>
          <?php
          }
          else{
          ?>
      <h1 class = "jersilver">WELCOME TO THE UNITY FAN PAGE</h1>
          <h2 class = "jersilver">Please enter your information below to join.</h2>
          <h5 class = "jersilver">Please note that the band and all of its entities and holdings values your privacy. We will engage in zero shenanigans when it comes to your personal information. Good day. </h5>
      <header class = "jersilver">Contact Form</header>
      <form name="form1" class="topBefore" method="post" action="fans.php">
        <p>
          <label class = "jersilver">Your Name:
            <input type="text" name="inName" id="inName" value="<?php echo $fanName;?>"><p class="red"><?php echo "$nameErrMsg";?></p>
          </label>
        </p>
        <p class = "jersilver">Your Email: 
          <input type="text" name="inEmail" id="inEmail" value="<?php echo $fanEmail;?>"/><p class="red"><?php echo "$emailErrMsg";?></p>
        </p>
        <p class = "jersilver">The Closest City To Me Is: <p class="red"><?php echo "$respErrMsg";?></p>
          <label>
            <select name="inCity" id="inCity">
              <option value="">Please Select a City</option>
              <option value="Des Moines"<?php if($fanCity == 'Des Moines'){echo("selected");}?>>Des Moines</option>
              <option value="Waterloo"<?php if($fanCity == 'Waterloo'){echo("selected");}?>>Waterloo</option>
              <option value="Cedar Rapids"<?php if($fanCity == 'Cedar Rapids'){echo("selected");}?>>Cedar Rapids</option>
            </select>
          </label>
            <script type="text/javascript">
            function validateMyForm() {
            if(!document.getElementById("honeypot").value) { 
              return true;
              } 
                else {
                  return false;
                }
              }
            </script>
            <div style="display:none;">
              <label>Keep this field blank</label>
              <input type="text" name="honeypot" id="honeypot" />
            </div>
        </p>
        <br>
        <p>
          <input type="submit" name="submit" id="submit" value="Submit" onsubmit="return validateMyForm();">
          <input type="reset" name="reset" id="reset" value="Reset">
        </p>
      </form>
    </div>
  </div>
      <footer class="row jersilver">
        <div class="large-12 columns"><hr>
          <p class="text-center jerfooterglow animated rubberBand">&copy; 2016 UNITY (All Rights Reserved)</p>
        </div>
      </footer>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation.min.js"></script>
<script>
  $(document).foundation();
</script>

      <?php
      }
      ?>

</body>
</html>