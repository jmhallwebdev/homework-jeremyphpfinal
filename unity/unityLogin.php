<?php 
session_cache_limiter('none');
session_start();
 
	if ($_SESSION['validUser'] == "yes")
	{
		$message = "Welcome Back!";	
	}
	else
	{
		if (isset($_POST['submitLogin']) )			//Was this page called from a submitted form?
		{
			$inUsername = $_POST['loginUsername'];
			$inPassword = $_POST['loginPassword'];
			
			include 'unityDbConnect.php';

			$sql = "SELECT userName,password FROM unity_admin WHERE userName = ? AND password = ?";				
			
			$query = $con->prepare($sql) or die("<p>SQL String: $sql</p>");
			
			$query->bind_param("ss",$inUsername,$inPassword);
			
			$query->execute() or die("<p>Execution </p>" );
			
			$query->bind_result($inUsername,$inPassword);
			
			$query->store_result();
			
			$query->fetch();	
			
			if ($query->num_rows == 1 )	
			{
				$_SESSION['validUser'] = "yes";
				$message = "Welcome Back: $inUsername";
			}
			else
			{
				$_SESSION['validUser'] = "no";					
				$message = "Sorry, there was a problem with your username or password. Please try again.";
			}			
			
			$query->close();
			$con->close();
			
		}//end if submitted

		else
		{
			
			//user needs to see form
		
		}	//end else submitted
		
	}//end else valid user
	
//turn off PHP and turn on HTML
?>
<!DOCTYPE html>
  <html class="no-js" lang="en">
  <head>
  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>UNITY - ADMIN PANEL</title>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/normalize.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.min.css">
  <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="css/index.css">
  <link rel="stylesheet" href="css/animate.css">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

  <style>
  	a {
	    color: silver;
	    text-shadow:
	    -1px -1px 0 #000,
	    1px -1px 0 #000,
	    -1px 1px 0 #000,
	    1px 1px 0 #000;  
		}
  </style>

</head>
<body class="jerbackgroundspace">
  <nav class="top-bar jergradient1" data-topbar>
    <ul class="title-area">
      <li class="name">
        <img class="jernavpic jerpaddingleft animated fadeInLeft" src="images/smallunitylogo2.png"/> <a href="index.html"><span class="jersilver"> OFFICIAL WEBSITE</span></a>
      </li>
      <li class="toggle-topbar menu-icon"><a href="index.html"><span><h4 class="jersilver"></h4></span></a></li>
    </ul>
    <section class="top-bar-section">
      <ul class="right animated fadeInRight">
      <li class="divider"></li>
        <li>
          <a href="fans.php"><h4 class="jersilver">Fans</h4></a>
        </li>
        <li class="divider"></li>
        <li>
          <a href="music.html"><h4 class="jersilver">Music</h4></a>
        </li>
        <li class="divider"></li>
        <li><a href="band.html"><h4 class="jersilver">The Band</h4></a></li>
        <li class="divider"></li>
        <li>
          <a href="merch.html"><h4 class="jersilver">Merch</h4></a>
        </li>
        <li class="divider"></li>
        <li>
          <a href="unityLogin.php"><h4 class="jersilver">Admin Login</h4></a>
        </li>
      </ul>
    </section>
  </nav>
  <div class="row">
	<h1 class="jersilver">UNITY - Administration Panel</h1>

	<h2  class="jersilver"><?php echo $message ?></h2>

	<?php

	if ($_SESSION['validUser'] == "yes")
	{
		
	?>
		<h3 class="jersilver">Administrator Options</h3>
        <p class="jersilver"><a href="http://www.jeremymhall.info/files/phpFinal/unityfinal/fans.php">INPUT NEW FAN</a></p>
        <br>
        <p class="jersilver"><a href="http://www.jeremymhall.info/files/phpFinal/unityfinal/seeFans.php">SEE ALL FANS</a></p>
		<br>
        <p class="jersilver"><a href="http://www.jeremymhall.info/files/phpFinal/unityfinal/seeFansDSM.php">SEE DES MOINES FANS</a></p>
        <p class="jersilver"><a href="http://www.jeremymhall.info/files/phpFinal/unityfinal/seeFansWLOO.php">SEE WATERLOO FANS</a></p>
        <p class="jersilver"><a href="http://www.jeremymhall.info/files/phpFinal/unityfinal/seeFansCR.php">SEE CEDAR RAPIDS FANS</a></p>
     	<br>
     	<br>
     	<br>
        <p class="jersilver"><a href="unityLogout.php">Logout of Admin Panel</a></p>	
        					
	<?php
	}

	else
		
	{
	?>
		<h2 class="jersilver">Please login below:</h2>
            <form method="post" name="login" action="unityLogin.php" >
              <p class="jersilver">Username: <input name="loginUsername" type="text" /></p>
              <p class="jersilver">Password: <input name="loginPassword" type="password" /></p>
              <p class="jersilver"><input name="submitLogin" value="Login" type="submit" /> <input name="" type="reset" />&nbsp;</p>
            </form>
                
	<?php //turn off HTML and turn on PHP
	}//end of checking for a valid user
					
	?>
<p class="jersilver"><a href = "http://www.jeremymhall.info/files/phpFinal/unityfinal/index.html">CLICK HERE TO RETURN TO THE HOME PAGE</a>

<footer class="row jersilver">
        <div class="large-12 columns"><hr>
          <p class="text-center jerfooterglow animated rubberBand">&copy; 2016 UNITY (All Rights Reserved)</p>
        </div>
      </footer>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation.min.js"></script>
<script>
  $(document).foundation();
</script>
</body>
</html>