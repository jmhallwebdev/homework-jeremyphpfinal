<?php 
session_cache_limiter('none');
session_start();
 
	if ($_SESSION['validUser'] == "yes")
	{

	include 'unityDbConnect.php';

	$event_id = "";
	
	$sql = "SELECT * FROM unity_fans WHERE fanCity='Cedar Rapids'";

	$res = $con->query($sql);
	
	if($res)
	{
		if ($res->num_rows > 0) 
		{
			$displayMsg = "<h1 class='jersilver'>Current # of registered fans: " . $res->num_rows . "</h1>";
			
			$displayMsg .= "<table id='jerTable'><tr id='heading'><td>NAME</td><td>EMAIL</td><td>CITY</td></tr>";
			while($row = $res->fetch_assoc()) 
			{
				$displayMsg .= "<td>";
				$displayMsg .= $row['fanName'].'</td><td>'.$row['fanEmail'].'</td><td>'.$row['fanCity'];
				$displayMsg .= "</td><td><a href=\"unityUpdateForm.php?fanNo=".$row['fanNo']."\">Update</a></td>";
				$displayMsg .= "</td><td><a href=\"unityDelete.php?fanNo=".$row['fanNo']."\">Delete</a></td>";
				$displayMsg .= "</tr>";
			}
			$displayMsg .= "</table>";
		} 
		else 
		{
			$displayMsg .= "0 results";
		}		
	}
	else
	{
		$displayMsg .= "<h3>Sorry there has been a problem</h3>";
		$displayMsg .= "<p>" . mysqli_error($con) . "</p>";
	}
	$con->close();
	
?>
<!DOCTYPE html>
  <html class="no-js" lang="en">
  <head>
  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>UNITY - ADMIN PANEL</title>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/normalize.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.min.css">
  <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="css/index.css">
  <link rel="stylesheet" href="css/animate.css">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <style>
  	a {
	    color: silver;
	    text-shadow:
	    -1px -1px 0 #000,
	    1px -1px 0 #000,
	    -1px 1px 0 #000,
	    1px 1px 0 #000;  
		}
  </style>
</head>
<body class="jerbackgroundspace">
  <nav class="top-bar jergradient1" data-topbar>
    <ul class="title-area">
      <li class="name">
        <img class="jernavpic jerpaddingleft animated fadeInLeft" src="images/smallunitylogo2.png"/> <a href="index.html"><span class="jersilver"> OFFICIAL WEBSITE</span></a>
      </li>
      <li class="toggle-topbar menu-icon"><a href="index.html"><span><h4 class="jersilver"></h4></span></a></li>
    </ul>
    <section class="top-bar-section">
      <ul class="right animated fadeInRight">
      <li class="divider"></li>
        <li>
          <a href="fans.php"><h4 class="jersilver">Fans</h4></a>
        </li>
        <li class="divider"></li>
        <li>
          <a href="music.html"><h4 class="jersilver">Music</h4></a>
        </li>
        <li class="divider"></li>
        <li><a href="band.html"><h4 class="jersilver">The Band</h4></a></li>
        <li class="divider"></li>
        <li>
          <a href="merch.html"><h4 class="jersilver">Merch</h4></a>
        </li>
        <li class="divider"></li>
        <li>
          <a href="unityLogin.php"><h4 class="jersilver">Admin Login</h4></a>
        </li>
      </ul>
    </section>
  </nav>
<body>
	<div class="row">
		<h1 class="jersilver">CEDAR RAPIDS FANS</h1>
		<div id="content">
		<?php echo $displayMsg; ?>
		</div>
	</div>
</body>

	<?php
	}
	else
	{
	    header('Location: unityLogin.php');
	}
	?>
<p class="jersilver"><a href = "http://www.jeremymhall.info/files/phpFinal/unityfinal/unityLogin.php">CLICK HERE TO RETURN TO THE ADMIN PAGE</a>
<p class="jersilver"><a href = "http://www.jeremymhall.info/files/phpFinal/unityfinal/index.html">CLICK HERE TO RETURN TO THE HOME PAGE</a>

<footer class="row jersilver">
        <div class="large-12 columns"><hr>
          <p class="text-center jerfooterglow animated rubberBand">&copy; 2016 UNITY (All Rights Reserved)</p>
        </div>
      </footer>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation.min.js"></script>
<script>
  $(document).foundation();
</script>
</body>
</html>