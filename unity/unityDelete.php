<?php 
session_cache_limiter('none');  //This prevents a Chrome error...
session_start();
 
	if ($_SESSION['validUser'] == "yes")		//is valid user?
	{

	include 'unityDbConnect.php';		//connects to the database

	$fanNo = "";

	$myFanNo = (int)$_GET['fanNo'];	//Pull the presenter_id from the GET parameter
		//echo "<p>The SQL Command: $myEvent_Id </p>";
	
	$sqlDel = "DELETE FROM unity_fans WHERE fanNo= ?";
		//echo "<p>The SQL Command: $sqlDel </p>";     //testing
	
	$queryDel = $con->prepare($sqlDel);	//prepare the statement
	
	$queryDel->bind_param(i,$myFanNo);	//bind the parameter to the statement
	
	if ( $queryDel->execute() )			//process the query
	{
		$message =  "<h1 class='jersilver'>Your record has been successfully deleted.</h1>";
		$message .= "<p class='jersilver'><a href='unityLogin.php'>RETURN</a> to the admin panel.</p>";	
	}
	else
	{
		$message = "<h1>You have encountered a problem with your delete.</h1>";
		$message .= "<h2 style='color:red'>" . mysqli_error($con) . "</h2>";
	}
	$queryDel->close();
	$con->close();	//close the database connection
?>


<!DOCTYPE html>
  <html class="no-js" lang="en">
  <head>
  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>UNITY - ADMIN PANEL</title>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/normalize.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.min.css">
  <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="css/index.css">
  <link rel="stylesheet" href="css/animate.css">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

  <style>
  	a {
	    color: silver;
	    text-shadow:
	    -1px -1px 0 #000,
	    1px -1px 0 #000,
	    -1px 1px 0 #000,
	    1px 1px 0 #000;  
		}
  </style>

</head>
<body class="jerbackgroundspace">
  <nav class="top-bar jergradient1" data-topbar>
    <ul class="title-area">
      <li class="name">
        <img class="jernavpic jerpaddingleft animated fadeInLeft" src="images/smallunitylogo2.png"/> <a href="index.html"><span class="jersilver"> OFFICIAL WEBSITE</span></a>
      </li>
      <li class="toggle-topbar menu-icon"><a href="index.html"><span><h4 class="jersilver"></h4></span></a></li>
    </ul>
    <section class="top-bar-section">
      <ul class="right animated fadeInRight">
      <li class="divider"></li>
        <li>
          <a href="fans.php"><h4 class="jersilver">Fans</h4></a>
        </li>
        <li class="divider"></li>
        <li>
          <a href="music.html"><h4 class="jersilver">Music</h4></a>
        </li>
        <li class="divider"></li>
        <li><a href="band.html"><h4 class="jersilver">The Band</h4></a></li>
        <li class="divider"></li>
        <li>
          <a href="merch.html"><h4 class="jersilver">Merch</h4></a>
        </li>
        <li class="divider"></li>
        <li>
          <a href="unityLogin.php"><h4 class="jersilver">Admin Login</h4></a>
        </li>
      </ul>
    </section>
  </nav>
  <div class="row">
	<h1 class="jersilver">UNITY - Administration Panel</h1>

<h2>
	<?php echo $message; ?>
</h2>

<?php 
}


	else
	{
	?>
	<h1>YOU NEED TO <a href="http://www.jeremymhall.info/files/phpFinal/unityfinal/unityLogin.php">LOGIN</a></h1>

	<?php 
	}
	?>

</body>
</html>

